import { Component, Output, EventEmitter, ContentChildren, QueryList, AfterContentInit, ElementRef, ViewChild, ViewChildren, AfterViewInit, ChangeDetectorRef, Renderer } from '@angular/core';

import { AuthRememberComponent } from '../auth-remember/auth-remember.component';
import { AuthMessageComponent } from '../auth-message/auth-message.component';

import { User } from './auth-form.interface';


@Component({
    selector: 'auth-form',
    templateUrl: './auth-form.component.html',
    styleUrls: ['./auth-form.component.scss']
})


export class AuthFormComponent implements AfterContentInit, AfterViewInit {
    showMessage: boolean;

    @ViewChild('email') email: ElementRef;
    @ViewChildren( AuthMessageComponent ) message: QueryList<AuthMessageComponent>;
    @ContentChildren( AuthRememberComponent ) remember: QueryList<AuthRememberComponent>;
    @Output() submitted: EventEmitter<User> = new EventEmitter<User>();

    constructor(
        private renderer: Renderer,
        private cd: ChangeDetectorRef
    ) {}

    ngAfterContentInit() {
        if ( this.remember) {
            this.remember.forEach( (item) => {
                item.checked.subscribe( ( checked: boolean ) => {
                    this.showMessage = checked;
                });
            });
        }
    }

    ngAfterViewInit() {
        this.renderer.setElementAttribute( this.email.nativeElement, 'placeholder', 'Entery your email address');
        this.renderer.setElementClass( this.email.nativeElement, 'email', true );
        this.renderer.invokeElementMethod( this.email.nativeElement, 'focus' );
        /*this.email.nativeElement.setAttribute('placeholder', 'Enter your email address');
        this.email.nativeElement.classList.add('email');
        this.email.nativeElement.focus();*/
        if ( this.message ) {
            this.message.forEach( (message) => {
                message.days = 30;
            });
            this.cd.detectChanges();
        }
    }

    onSubmit( value: User ) {
        this.submitted.emit( value );
    }

}
