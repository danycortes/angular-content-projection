import { Component } from '@angular/core';
import { User } from './auth-form/auth-form.interface';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})


export class AppComponent {
    rememberMe: boolean = false;

    createUser( user: User ) {
        console.log( 'Create account for: ', user );
    }

    loginUser( user: User ) {
        console.log('Login', user, this.rememberMe );
    }

    rememberUser( remember: boolean ) {
        this.rememberMe = remember;
    }
}
